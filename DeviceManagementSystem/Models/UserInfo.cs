﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DeviceManagementSystem.Models
{
    public class UserInfo
    {
        public int UserID { get; set; }
        public string UserName { get; set; }
        public string UserRole { get; set; }
        public string UserLocation { get; set; }

    }
}