﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DeviceManagementSystem.Models
{
    public class Device
    {
        public int DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string Manufacturer { get; set; }
        public string DeviceType { get; set; }
        public string OS { get; set; }
        public string OSVersion { get; set; }
        public string Processor { get; set; }
        public string RAM { get; set; }
        public int UserID { get; set; }

    }
}