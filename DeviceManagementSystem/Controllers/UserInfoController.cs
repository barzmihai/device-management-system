﻿using DeviceManagementSystem.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DeviceManagementSystem.Controllers
{
    public class UserInfoController : ApiController
    {
        public HttpResponseMessage Get()
        {
            string query = @"select UserID,UserName,UserRole,UserLocation 
                            from dbo.UserInfo";
            DataTable table = new DataTable();
            using (var con = new SqlConnection(ConfigurationManager.
                ConnectionStrings["DeviceManagementSystemDB"].ConnectionString))
            using (var cmd = new SqlCommand(query, con))
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.Text;
                da.Fill(table);
            }

            return Request.CreateResponse(HttpStatusCode.OK, table);
        }

        public HttpResponseMessage GetID(int id)
        {
            string query = @"select UserID,UserName,UserRole,UserLocation 
                            from dbo.UserInfo
                            where UserID=" + id;
            DataTable table = new DataTable();
            using (var con = new SqlConnection(ConfigurationManager.
                ConnectionStrings["DeviceManagementSystemDB"].ConnectionString))
            using (var cmd = new SqlCommand(query, con))
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.Text;
                da.Fill(table);
            }

            return Request.CreateResponse(HttpStatusCode.OK, table);
        }

        public string Post(UserInfo userInfo)
        {
            try
            {
                string query = @"insert into dbo.UserInfo 
                                values ('"+userInfo.UserName+@"','"+userInfo.UserRole+ @"','" + userInfo.UserLocation + @"')";
                DataTable table = new DataTable();
                using (var con = new SqlConnection(ConfigurationManager.
                    ConnectionStrings["DeviceManagementSystemDB"].ConnectionString))
                using (var cmd = new SqlCommand(query, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.Text;
                    da.Fill(table);
                }
                return "Added Successfully!";
            }
            catch
            {
                return "Failed to add!";
            }
        }

        public string Put(UserInfo userInfo)
        {
            try
            {
                string query = @"update dbo.UserInfo set UserName='" + userInfo.UserName + @"',UserRole='" + userInfo.UserRole + @"',UserLocation='" + userInfo.UserLocation + @"'
                                where UserID="+userInfo.UserID;
                DataTable table = new DataTable();
                using (var con = new SqlConnection(ConfigurationManager.
                    ConnectionStrings["DeviceManagementSystemDB"].ConnectionString))
                using (var cmd = new SqlCommand(query, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.Text;
                    da.Fill(table);
                }
                return "Updated Successfully!";
            }
            catch
            {
                return "Failed to update!";
            }
        }

        public string Delete(int id)
        {
            try
            {
                string query = @"delete from dbo.UserInfo
                                where UserID=" + id;
                DataTable table = new DataTable();
                using (var con = new SqlConnection(ConfigurationManager.
                    ConnectionStrings["DeviceManagementSystemDB"].ConnectionString))
                using (var cmd = new SqlCommand(query, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.Text;
                    da.Fill(table);
                }
                return "Deleted Successfully!";
            }
            catch
            {
                return "Failed to delete!";
            }
        }
    }
}
