﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DeviceManagementSystem.Models;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace DeviceManagementSystem.Controllers
{
    public class DeviceController : ApiController
    {
        public HttpResponseMessage Get()
        {
            string query = @"select DeviceID,DeviceName,Manufacturer,DeviceType,OS,OSVersion,Processor,RAM,UserID 
                            from dbo.Device";
            DataTable table = new DataTable();
            using (var con = new SqlConnection(ConfigurationManager.
                ConnectionStrings["DeviceManagementSystemDB"].ConnectionString))
            using (var cmd = new SqlCommand(query, con))
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.Text;
                da.Fill(table);
            }

            return Request.CreateResponse(HttpStatusCode.OK, table);
        }

        public HttpResponseMessage GetID(int id)
        {
            string query = @"select DeviceID,DeviceName,Manufacturer,DeviceType,OS,OSVersion,Processor,RAM,UserID 
                            from dbo.Device
                            where DeviceID=" + id;
            DataTable table = new DataTable();
            using (var con = new SqlConnection(ConfigurationManager.
                ConnectionStrings["DeviceManagementSystemDB"].ConnectionString))
            using (var cmd = new SqlCommand(query, con))
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.Text;
                da.Fill(table);
            }

            return Request.CreateResponse(HttpStatusCode.OK, table);
        }

        public string Post(Device device)
        {
            try
            {
                string query = @"insert into dbo.Device 
                                values ('" 
                                + device.DeviceName + @"','" 
                                + device.Manufacturer + @"','" 
                                + device.DeviceType + @"','" 
                                + device.OS + @"','" 
                                + device.OSVersion + @"','" 
                                + device.Processor + @"','" 
                                + device.RAM + @"','" 
                                + device.UserID + @"')";
                DataTable table = new DataTable();
                using (var con = new SqlConnection(ConfigurationManager.
                    ConnectionStrings["DeviceManagementSystemDB"].ConnectionString))
                using (var cmd = new SqlCommand(query, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.Text;
                    da.Fill(table);
                }
                return "Added Successfully!";
            }
            catch
            {
                return "Failed to add!";
            }
        }

        public string Put(Device device)
        {
            try
            {
                string query = @"update dbo.Device 
                                set DeviceName='" + device.DeviceName + 
                                @"',Manufacturer='" + device.Manufacturer + 
                                @"',DeviceType='" + device.DeviceType + 
                                @"',OS='" + device.OS +
                                @"',OSVersion='" + device.OSVersion +
                                @"',Processor='" + device.Processor +
                                @"',RAM='" + device.RAM +
                                @"' where DeviceID=" + device.DeviceID;
                DataTable table = new DataTable();
                using (var con = new SqlConnection(ConfigurationManager.
                    ConnectionStrings["DeviceManagementSystemDB"].ConnectionString))
                using (var cmd = new SqlCommand(query, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.Text;
                    da.Fill(table);
                }
                return "Updated Successfully!";
            }
            catch
            {
                return "Failed to update!";
            }
        }

        public string Delete(int id)
        {
            try
            {
                string query = @"delete from dbo.Device
                                where DeviceID=" + id;
                DataTable table = new DataTable();
                using (var con = new SqlConnection(ConfigurationManager.
                    ConnectionStrings["DeviceManagementSystemDB"].ConnectionString))
                using (var cmd = new SqlCommand(query, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.Text;
                    da.Fill(table);
                }
                return "Deleted Successfully!";
            }
            catch
            {
                return "Failed to delete!";
            }
        }
    }
}
