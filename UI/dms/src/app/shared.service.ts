import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 
import { Observable, observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
readonly APIUrl="http://localhost:52824/api";
  constructor(private http:HttpClient) { }

  getDeviceList():Observable<any[]>{
    return this.http.get<any>(this.APIUrl+'/device');
  }

  addDevice(val:any){
    return this.http.post(this.APIUrl+'/device',val);
  }

  updateDevice(val:any){
    return this.http.put(this.APIUrl+'/device',val);
  }

  deleteDevice(val:any){
    return this.http.delete(this.APIUrl+'/device/'+val);
  }

  showDeviceDetails(val:any){
    return this.http.get(this.APIUrl+'/device/'+val);
  }
}
