import { Component, OnInit, Input} from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, AbstractControl} from '@angular/forms';
import { SharedService } from 'src/app/shared.service';
import { ShowDevComponent } from '../show-dev/show-dev.component';

@Component({
  selector: 'app-add-edit-dev',
  templateUrl: './add-edit-dev.component.html',
  styleUrls: ['./add-edit-dev.component.css']
})
export class AddEditDevComponent implements OnInit {

  constructor(private service:SharedService) { }

  @Input() device:any;
  DeviceID:any;
  DeviceName:string;
  Manufacturer:string;
  DeviceType:string;
  OS:string;
  OSVersion:string;
  Processor:string;
  RAM:string;
  UserID:any;

  ngOnInit(): void {
    this.DeviceID=this.device.DeviceID;
    this.DeviceName=this.device.DeviceName;
    this.Manufacturer=this.device.Manufacturer;
    this.DeviceType=this.device.DeviceType;
    this.OS=this.device.OS;
    this.OSVersion=this.device.OSVersion;
    this.Processor=this.device.Processor;
    this.RAM=this.device.RAM;
    this.UserID=this.device.UserID;
  }

  addDevice(){
    var x = {DeviceID:this.DeviceID,
            DeviceName:this.DeviceName,
            Manufacturer:this.Manufacturer,
            DeviceType:this.DeviceType,
            OS:this.OS,
            OSVersion:this.OSVersion,
            Processor:this.Processor,
            RAM:this.RAM,
            UserID:this.UserID};
    this.service.addDevice(x).subscribe(res=>{
      alert(res.toString());
    });
  }

  updateDevice(){
    var x = {DeviceID:this.DeviceID,
      DeviceName:this.DeviceName,
      Manufacturer:this.Manufacturer,
      DeviceType:this.DeviceType,
      OS:this.OS,
      OSVersion:this.OSVersion,
      Processor:this.Processor,
      RAM:this.RAM,
      UserID:this.UserID};
    this.service.updateDevice(x).subscribe(res=>{
    alert(res.toString());
    });
  }

  
}
