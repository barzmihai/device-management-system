import { Component, OnInit } from '@angular/core';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-show-dev',
  templateUrl: './show-dev.component.html',
  styleUrls: ['./show-dev.component.css']
})
export class ShowDevComponent implements OnInit {

  constructor(private service:SharedService) { }

  DeviceList:any=[];

  ModalTitle:string="";
  ActivateAddEditDeviceComp:boolean=false;
  EditVar:boolean=false;
  device:any;

  ngOnInit(): void {
    this.refreshDeviceList();
  }

  addClick(){
    this.device={
      DeviceID:0,
      DeviceName:"",
      Manufacturer:"",
      DeviceType:"",
      OS:"",
      OSVersion:"",
      Processor:"",
      RAM:"",
      UserID:0
    }
    this.ModalTitle="Add Device";
    this.ActivateAddEditDeviceComp=true;
  }

  editClick(item:any){
    this.device=item;
    this.ModalTitle="Edit Device";
    this.ActivateAddEditDeviceComp=true;
    this.EditVar=true;
  }

  closeClick(){
    this.ActivateAddEditDeviceComp=false;
    this.refreshDeviceList();
  }

  viewClick(item:any){
    this.device=item;
    this.ModalTitle="Device Details";
    this.ActivateAddEditDeviceComp=true;
  }

  deleteClick(item:any){
    if (confirm('Are you sure?')){
      this.service.deleteDevice(item.DeviceID).subscribe(data=>{
        alert(data.toString());
        this.refreshDeviceList();
      });
    }
  }

  refreshDeviceList(){
    this.service.getDeviceList().subscribe(data=>{
      this.DeviceList=data;
    });
  }

}
